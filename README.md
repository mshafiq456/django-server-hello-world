# README #

This is the code for my simple Django server. In order to use it, you will need python 3 and django package installed.
To run ther server, cd in to project's root folder and run "python ./website/manage.py runserver". If you don't have a virtual environment setup with python 3, you may have to use the command, "python3 ./website/manage.py runserver". 